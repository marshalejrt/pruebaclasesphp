<!DOCTYPE html>
<html>
    <head>
        <title>Laravel</title>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
        <link href="https://fonts.googleapis.com/css?family=Lato:100" rel="stylesheet" type="text/css">
        <!-- Bootstrap Load -->
        <link href="css/bootstrap-3.3.6-dist/css/bootstrap.min.css" rel="stylesheet">
        <script src="js/jquery.min.js"></script>
        <script src="js/bootstrap.min.js"></script>
        <script src="js/FuncionesGenerales.js"></script>
    </head>
    <body>
    <div class="row">
        <div class="container text-center">
            <h4>Prueba de Clases para GEOPAGO</h4>
            <p>Figuras</p>

        </div>
    </div>
        <div class="container">
            <p>Ingrese los valores correspondientes y seleccione el tipo de figura<br>para crearla</p>
            <form  action="{{route('generarfigura')}}" method="post">
                <input type="hidden" value="{{csrf_token()}}" name="_token">
                <div class="form-row">
                    <div class="form-group col-md-4">
                        <label for="base" class="col-form-label">Base</label>
                        <input type="number" step="any" class="form-control" id="base" name="base" placeholder="Base" value="{{$datos['base']}}">
                    </div>
                    <div class="form-group col-md-4">
                        <label for="altura" class="col-form-label">Altura</label>
                        <input type="number" step="any" class="form-control" id="altura" name="altura" placeholder="altura" value="{{$datos['altura']}}">
                    </div>
                    <div class="form-group col-md-4">
                        <label for="diametro" class="col-form-label">Diametro</label>
                        <input type="number" step="any" class="form-control" id="diametro" name="diametro" placeholder="Diametro" value="{{$datos['diametro']}}">
                    </div>
                    <div class="form-group col-md-4">
                        <label for="figura" class="col-form-label">Seleccione el Tipo de Figura</label>
                        <select class="form-control" name="figura" id="figura" value="{{$datos['tipo']}}">
                            <option value="Circulo">Circulo</option>
                            <option value="Cuadrado">Cuadrado</option>
                            <option value="Triangulo">Triangulo</option>
                        </select>
                    </div>
                    <div class="form-group col-md-4">
                        <button type="submit" class="btn btn-primary">Generar Figura</button>
                    </div>
                </div>

            </form>
        </div>
    <div class="row" id="respuesta" style="{{$resp}}">
        <div class="container text-center">
            <p><strong>{{$figura['tipo']}}</strong> Creado Correctamente!</p>
                <div class="form-row">
                    <div class="form-group col-md-4">
                        <label for="base" class="col-form-label">Base</label>
                        <input type="number" disabled step="any" class="form-control" id="base" name="base" placeholder="null" value="{{$figura['base']}}">
                    </div>
                    <div class="form-group col-md-4">
                        <label for="altura" class="col-form-label">Altura</label>
                        <input type="number" disabled step="any" class="form-control" id="altura" name="altura" placeholder="null" value="{{$figura['altura']}}">
                    </div>
                    <div class="form-group col-md-4">
                        <label for="diametro" class="col-form-label">Diametro</label>
                        <input type="number" disabled step="any" class="form-control" id="diametro" name="diametro" placeholder="null" value="{{$figura['diametro']}}">
                    </div>
                    <div class="form-group col-md-4">
                        <label for="superficie" class="col-form-label">Superficie</label>
                        <input type="number" disabled step="any" class="form-control" id="superficie" name="superficie" placeholder="null" value="{{$figura['superficie']}}">
                    </div>
                </div>

            </form>
        </div>
        <div class="row">
            <div class="container text-center">
                <div class="figura" style="display: none"></div>
            </div>
        </div>
    </div>
    <style>
        .Circulo {
            width: 100px;
            height: 100px;
            -moz-border-radius: 50%;
            -webkit-border-radius: 50%;
            border-radius: 50%;
            background: #5cb85c;
            display: block;
        }
        .Triangulo{
            width: 0;
            height: 0;
            border-right: 100px solid transparent;
            border-top: 100px solid transparent;
            border-left: 100px solid transparent;
            border-bottom: 100px solid #f0ad4e;
            display: block;
        }
        .Cuadrado {
            width: 100px;
            height: 100px;
            border: 3px solid #555;
            background: #428bca;
            display: block;
        }
    </style>
    <script>
        $('#figura').val('{{$datos['tipo']}}');
        $(document).ready(function () {

            $('.figura').addClass('{{$datos['tipo']}}');
            $('.figura').show(200);
        });

    </script>

    <div class="row text-center">
        Copyright <a href="http://indexsoftwareca.com/ejdev/" target="_blank">Ejrodriguez</a> para GEOPAGO - 2017
    </div>
    </body>

</html>
