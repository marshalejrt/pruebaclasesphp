<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {
    $datos=array('tipo'=>'Circulo','base'=>null,'altura'=>null,'diametro'=>null);
    $figura=array('tipo'=>null,'base'=>null,'altura'=>null,'superficie'=>null,'diametro'=>null);
    $resp='display:none';
    return view('welcome')->with(['figura'=>$figura,'datos'=>$datos,'resp'=>$resp]);
});


Route::post('/generarfigura', [
    'uses' => 'ControladorFiguras@postGenerarFigura',
    'as' => 'generarfigura'
]);