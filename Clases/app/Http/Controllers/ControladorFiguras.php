<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Clases;

class ControladorFiguras extends Controller
{
    /**
     * postGenerarFigura
     * Recibe los parametros y llama las clases para generar las figuras correspondientes
     * @param  request
     * @return informacion
     */
    public function postGenerarFigura(Request $request)
    {
        //instanciamos la Clase mediante el Factory
        $figuraGeometrica = new Clases\FigurasFactory();
        //Creamos la Figura
        $objeto =$figuraGeometrica->Create($request['figura'],$request['base'],$request['altura'],$request['diametro']);
        //Obtenemos sus propiedades
        $figura=$objeto->getAtributos();
        $datos=array('tipo'=>$request['figura'],'base'=>$request['base'],'altura'=>$request['altura'],'diametro'=>$request['diametro']);
        $resp='';
        //devolvemos el resultado
        return view('welcome')->with(['figura'=>$figura,'datos'=>$datos,'resp'=>$resp]);

    }


}
