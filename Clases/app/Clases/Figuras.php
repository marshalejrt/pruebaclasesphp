<?php

namespace App\Clases;


class Figuras
{

    private $tipo;
    private $base;
    private $altura;
    private $diametro;


    /**
     * @param null $tipo
     * @param null $base
     * @param null $altura
     * @param null $diametro
     */
    public function __construct($tipo=null, $base=null, $altura=null, $diametro=null)
    {
        $this->tipo = $tipo;
        $this->base = $base;
        $this->altura = $altura;
        $this->diametro = $diametro;
    }


    public function getBase(){
        return $this->base;
    }

    public function getAltura(){
        return $this->altura;
    }

    public function getDiametro(){
        return $this->diametro;
    }

    public function getTipo(){
        return $this->tipo;
    }

    public function getAtributos(){
        return array(
            'tipo' => $this->getTipo(),
            'base' => $this->getBase(),
            'altura'=>$this->getAltura(),
            'superficie' => $this->getSuperficie(),
            'diametro' => $this->getDiametro()
        );
    }






}
