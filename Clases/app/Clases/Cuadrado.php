<?php

namespace App\Clases;


class Cuadrado extends Figuras implements implementFiguras
{

    /**
     * @param null $base
     * @param null $altura
     */
    function __construct($base, $altura)
    {
        parent::__construct('Cuadrado', $base, $altura, null);
    }

    /**
     * @return number
     */
    public function getSuperficie()
    {
        return pow($this->getBase(), 2);

    }
}