<?php

namespace App\Clases;


class FigurasFactory
{
    /**
     * @param null $tipo
     * @param null $base
     * @param null $altura
     * @param null $diametro
     * @return Circulo|Cuadrado|Triangulo|string
     */
    public function Create($tipo = null, $base = null, $altura = null, $diametro = null){

        switch ($tipo) {
            case 'Circulo':
                return new Circulo($diametro);
                break;
            case 'Triangulo':
                return new Triangulo($base, $altura);
                break;
            case 'Cuadrado':
                return new Cuadrado($base, $altura);
                break;
            default:
                return "Ha indicado una figura inv�lida";
                break;
        }

    }
}