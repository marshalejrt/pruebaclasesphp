<?php


namespace App\Clases;


class Triangulo extends Figuras implements implementFiguras
{

    /**
     * @param null $base
     * @param null $altura
     */
    function __construct($base, $altura)
    {
        parent::__construct('Triangulo', $base, $altura, null);
    }

    /**
     * @return float
     */
    public function getSuperficie(){
        return ($this->getBase() * $this->getAltura()) / 2;
    }

}