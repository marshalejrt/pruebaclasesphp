<?php

namespace App\Clases;


interface implementFiguras
{
    public function getTipo();
    public function getSuperficie();
    public function getBase();
    public function getAltura();
    public function getDiametro();
    public function getAtributos();
}