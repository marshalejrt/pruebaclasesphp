<?php

namespace App\Clases;


class Circulo extends Figuras implements implementFiguras
{
    /**
     * @param null $diametro
     */
    public function __construct($diametro)
    {
        parent::__construct('Circulo',null,null,$diametro);
    }

    /**
     * @return float
     */
    public function getSuperficie(){
        $ratio = $this->getDiametro() / 2;
        return 3.14 * pow($ratio, 2);
    }
}